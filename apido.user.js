// ==UserScript==
// @name        Apido
// @namespace   http://drupal.org/project/apido
// @description Code sample enhancements for api.drupal.org
// @author      Corey Aufang
// @include     http://api.drupal.org/*
// @match       http://api.drupal.org/*
// @include     http://drupalcontrib.org/*
// @match       http://drupalcontrib.org/*
// @include     http://drupalcode.org/*
// @match       http://drupalcode.org/*
// @version     0.6
// ==/UserScript==

if (!this.GM_getValue || (this.GM_getValue.toString && this.GM_getValue.toString().indexOf("not supported")>-1)) {
  this.GM_getValue=function (key,def) {
      return localStorage[key] || def;
  };
  this.GM_setValue=function (key,value) {
      return localStorage[key]=value;
  };
  this.GM_deleteValue=function (key) {
      return delete localStorage[key];
  };
}

(function () {
  var callback = function () {
    
    var apido = function ($) {
      $(function () {
        $('<style type="text/css">[apido]{cursor:pointer;}</style>').appendTo("html head");
        
        var matches = $('<style type="text/css" />').appendTo("html head"),
            pattern_match = "",
            bracket_stack = [],
            bracket_count = 0,
            code = $("pre.php, .page_body > div.pre", document);
        
        code.find('code, span.hl.sym').contents().each(function () {
          // check if nodeType is text, which means not a comment or any other tag; http://www.w3.org/TR/DOM-Level-2-Core/core.html#ID-111237558
          if (this.nodeType == 3) {
            // replacing text in textNodes with html; http://stackoverflow.com/questions/2561266/prevent-escaping-special-characters-on-textnodejs-html/2561495#2561495
            var pattern = /[{}\[\]()]/;
            var node = this /* Text node you want to replace strings in */;
  
            while (pattern_match = pattern.exec(node.data)) {
                var bracket = document.createElement('span');
                bracket.className = 'php-bracket';
                bracket.innerHTML = pattern_match;
                
                node= node.splitText(pattern_match.index);
                node= node.splitText(pattern_match[0].length);
                node.parentNode.replaceChild(bracket, node.previousSibling);
            };
  
          }
        });
        
        // iterate through brackets and assign matching number
        code.find('span.php-bracket').each(function () {
          var $this = $(this);
          if (this.innerHTML.match(/[{\[(]/)) {
            bracket_count++;
            bracket_stack.push(bracket_count);
            $this.attr('apido', 'bracket-' + bracket_count);
          }
          else {
            // @todo need to fix the poping to for only of same type
            $this.attr('apido', 'bracket-' + bracket_stack.pop());
          }
        });
        
        code.find('span.php-variable, span.hl.kwc').each(function () {
          var $this = $(this);
          $this.attr('apido', $this.text());
        });
        
        code.click(function (e) {
          var $target = $(e.target);
          if (attr_apido = $target.attr('apido')) {
            
            var match_selector = '[apido="' + attr_apido + '"]';
            
            var css = match_selector + '{background-color:#F88;}';
            css += match_selector + '.apido-first{background-color:#F00;}';
            
            var first = $(match_selector).eq(0).addClass('apido-first');
            
            matches[0].innerHTML = css;
            
            // if metaKey is pressed while clicking, take to first instance; metaKey was not working on drupalcode.org
            if (e.metaKey || e.ctrlKey) {
              $(window).scrollTop(first.offset().top-10);
            }
            
          }
        });
      });
    };
    
    if (typeof jQuery == 'undefined') {
      var jquery_script = document.createElement("script");
      jquery_script.setAttribute("src", "//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
      jquery_script.addEventListener('load', function() {
        apido(jQuery);
      }, false);
      document.body.appendChild(jquery_script);
    }
    else {
      apido(jQuery);
    }
    
  };
  
  var script = document.createElement("script");
  script.textContent = "(" + callback.toString() + ")();";
  document.body.appendChild(script);
})();

/**
 * Check for new Apido versions.
 * 
 * GM functions can be invoked from GM environment only.
 */
(function () {
  if (typeof GM_xmlhttpRequest != 'function' || typeof window.google != 'undefined') {
    return;
  }
  // default value here should match @version in comments at top
  var version = GM_getValue('version', '0.6');
  var lastChecked = GM_getValue('update.last', 0);
  var now = parseInt(new Date() / 1000, 10);
  // Check every 3 days.
  var interval = 60 * 60 * 24 * 3;
  if (lastChecked - now < -interval) {
    // Whatever happens to this request, remember that we tried.
    GM_setValue('update.last', now);
    GM_xmlhttpRequest({
      method: 'GET',
      url: 'http://drupalcode.org/project/apido.git/blob_plain/HEAD:/apido.user.js',
      onload: function (responseDetails) {
        if (responseDetails.status == 200) {
          var newversion = responseDetails.responseText.match(/@version +([0-9.]+)/i)[1];
          if (newversion == version) {
            return;
          }
          var doUpdate = window.confirm('A new version of Apido is available. Shall we visit the project page to update?');
          if (doUpdate) {
            window.open('http://drupal.org/project/apido', 'apido');
            // Let's just assume that we DID update. ;)
            //GM_setValue('version', newversion);
          }
        }
      }
    });
  }
})();
